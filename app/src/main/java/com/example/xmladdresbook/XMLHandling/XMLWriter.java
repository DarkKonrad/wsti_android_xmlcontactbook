package com.example.xmladdresbook.XMLHandling;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import com.example.xmladdresbook.model.ContactModel;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class XMLWriter  {

    DocumentBuilderFactory dbFactory;
    DocumentBuilder dbBuilder;
    List <XmlNode> listOfNodes;

    private static XMLWriter xmlWriter = new XMLWriter();

    public static XMLWriter getInstance() {
        return  xmlWriter;
    }

    public void addXmlNodesToWrite(XmlNode[] nodes){
        for (XmlNode node : nodes){
            listOfNodes.add(node);
        }
    }
    private  XMLWriter(){
        dbFactory = DocumentBuilderFactory.newInstance();
        listOfNodes = new ArrayList<XmlNode>();
    }

    // Unlike reading XML file, we can write one or many elements,
    // can do by low effort two methods,
    // One 'normal' and one async.



    public void write(OutputStream output){
        try{

            dbBuilder = dbFactory.newDocumentBuilder();
            Document  doc = dbBuilder.newDocument();

            Element  rootElement = doc.createElementNS(
                       "https://gitlab.com/DarkKonrad/wsti_android_xmlcontactbook",
                       "ContactBook");

            doc.appendChild(rootElement);


            for(XmlNode object : listOfNodes){
                if(object == null)
                    continue;
                rootElement.appendChild(object.getXmlNode(doc));
            }

            listOfNodes.clear();

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT,"yes");
            DOMSource source = new DOMSource(doc);

            StreamResult resultOutput = new StreamResult(output);

            transformer.transform(source,resultOutput);

        }
       catch (Exception e){
           Log.e("XMLWriter: Write error", e.getMessage());
            e.printStackTrace();
       }

    }



    public void write(Context context, OutputStream output){
        ProgressDialog dialog = new ProgressDialog(context);

        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();

        write(output);

        dialog.dismiss();
    }



    // AsyncTask Methods

    public void writeAsync(Context context, OutputStream output, XmlNode... objectsToWrite)  {
        WriteAsync asyncWriter = new WriteAsync(context,output);
        asyncWriter.execute();
    }

    class WriteAsync extends AsyncTask<Void,Void,OutputStream>{
        OutputStream output;
        ProgressDialog dialog;

        public WriteAsync(Context context,OutputStream ostream){
            dialog = new ProgressDialog(context);
            output = ostream;
        }

        @Override
        protected OutputStream doInBackground(Void... voids) {
            // It may be confusing. Goal is to bypass AsyncTask argument limit,
            // and to debug log to 'console'OutputResult (XML contents)
            // So, we return output given by user, setted by Constructor of this class.

            write(output);
            return output;
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(OutputStream result){
            super.onPostExecute(result);
            dialog.dismiss();
            Log.wtf("\nAsync Write Result: ",result.toString());
        }




    }


}
