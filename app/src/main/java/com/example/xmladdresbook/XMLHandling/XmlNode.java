package com.example.xmladdresbook.XMLHandling;


import org.w3c.dom.Document;
import org.w3c.dom.Node;

public interface XmlNode {
    Node getXmlNode(Document doc);
}
