package com.example.xmladdresbook.XMLHandling;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;

import com.example.xmladdresbook.Collections.ContactCollection;
import com.example.xmladdresbook.Collections.GroupCollection;
import com.example.xmladdresbook.model.ContactModel;
import com.example.xmladdresbook.model.Group;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;


// Since this is educational project, I allow myself to let's say "concretize" XML reader"
// Generic approach could be possible by using java.util.function.Function;
public class XMLReader {

    private  ContactModel.ContactModelBuilder contactModelBuilder;
    private  Group.GroupBuilder groupBuilder;

    // Indicates that we are inside <Contact> or <Group> Tag
    private boolean isContactTag = false;
    private boolean isGroupTag = false;
    private boolean areAttrsCollected = false;

    private String readerResult="";

    private XMLReader(){}
    private static XMLReader reader = new XMLReader();
    public static  XMLReader getInstance(){return  reader;}

    private String _read(InputStream input){
        boolean stopAfterEnd_Tag = false;

        StringBuilder builder = new StringBuilder();

        ContactCollection contactCollection = ContactCollection.getInstance();
        GroupCollection groupCollection = GroupCollection.getInstance();

        this.contactModelBuilder = new ContactModel.ContactModelBuilder();
        this.groupBuilder = new Group.GroupBuilder();

        try{
            // Need to consider if it is needed
            // it was good idea on start when it was not decided how it will actually behave
            // at this moment idea is that collection will be refilled from XML but it will be
            // leaved 'as is' just in case
            contactCollection.makeBackup();
            contactCollection.removeAll();
            groupCollection.makeBackup();
            groupCollection.removeAll();

            XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xmlPullParserFactory.newPullParser();
            parser.setInput(input,null);

            int eventType = parser.getEventType();
            String nodeName ="";
            // it gonna be long..
            while(eventType != XmlPullParser.END_DOCUMENT){

                switch (eventType){

                    case XmlPullParser.START_DOCUMENT:
                        builder.append("\nSTART_DOCUMENT");
                        break;

                    case XmlPullParser.END_DOCUMENT:
                        builder.append("\nEND_DOCUMENT");
                        break;

                    case XmlPullParser.START_TAG:
                        stopAfterEnd_Tag = false;
                        nodeName = parser.getName();
                        builder.append(startTagHandler(parser,contactModelBuilder,groupBuilder));
                        break;

                    case XmlPullParser.END_TAG:
                        stopAfterEnd_Tag = true;
                        builder.append(endTagHandler(parser, contactModelBuilder,groupBuilder));
                        break;

                        // Werid behaviour, it seems that after endTag is goes again to Text event..
                    case XmlPullParser.TEXT:
                        if(stopAfterEnd_Tag == false)
                            builder.append(textTagHandler(parser,nodeName,contactModelBuilder,groupBuilder));
                        break;

                    default:
                        break;
                }
                eventType = parser.next();

            }

        }
        catch (Exception ex){
            contactCollection.restoreBackup();
            groupCollection.restoreBackup();

            Log.e("XmlReader: Read Error", ex.getMessage());
            ex.printStackTrace();

        }

        return builder.toString();
    }

    private String textTagHandler(XmlPullParser parser,
                                  String nodeName,
                                  ContactModel.ContactModelBuilder contactBuilder,
                                  Group.GroupBuilder groupBuilder){

        StringBuilder stringBuilder = new StringBuilder();

        if(isContactTag == true){

            stringBuilder.append(contactAddFragment(parser,nodeName,contactBuilder));
            return  stringBuilder.toString();
        }

        if(isGroupTag == true){
            stringBuilder.append(groupAddFragment(parser,nodeName,groupBuilder));
            return stringBuilder.toString();
        }

        return stringBuilder.toString();
    }

    private String startTagHandler(XmlPullParser parser,
                                   ContactModel.ContactModelBuilder contactBuilder,
                                   Group.GroupBuilder groupBuilder){

       String nodeName = parser.getName();
       StringBuilder stringBuilder = new StringBuilder();

        if(nodeName.equals(ContactModel.getTagName())){
            // Mark that we started Contact Tag
            this.isContactTag = true;
            this.isGroupTag = false;

            // We expect attrs only in 'Main Tags': <Contact> and <Group>
            if(this.areAttrsCollected == false){ // Considering to remove this flag
                stringBuilder.append(getAttributesContact(parser,contactBuilder));
                this.areAttrsCollected = true;
            }
        }

        // Since attr should be check only once, and there are lot of contact's child tags
        // we need to know that we are inside <contact>


         if(nodeName.equals(Group.getTagName())){
            this.isGroupTag = true;
            this.isContactTag =false;

            if(this.areAttrsCollected == false){
                stringBuilder.append(getAttributesGroup(parser,groupBuilder));
                this.areAttrsCollected = true;
            }
        }



        return stringBuilder.toString();
    }

    private String getAttributesGroup(XmlPullParser parser,
                                      Group.GroupBuilder groupBuilder){

        StringBuilder stringBuilder = new StringBuilder();
        String name = parser.getName();

        stringBuilder.append("\nSTART--Group");

        if(name !=null){
            int size = parser.getAttributeCount();

            for(int i =0;i<size;i++){
                String attrName = parser.getAttributeName(i);
                if(attrName.equals("id")){
                    int id = Integer.parseInt(parser.getAttributeValue(i));
                    groupBuilder.setId(id);
                    stringBuilder.append("\nGroup Attribute: id | Value: ").append(id);
                }

            }
        }
        return stringBuilder.toString();
    }

    private String endTagHandler(XmlPullParser parser,
                                 ContactModel.ContactModelBuilder contactModelBuilder,
                                 Group.GroupBuilder groupBuilder){

        String nodeName = parser.getName();
        StringBuilder stringBuilder = new StringBuilder();

        // if we reached i.e. end of Contact Tag, we should make whole contact,
        // otherwise, do nothing
        if(nodeName.equals(ContactModel.getTagName())){
            ContactCollection.getInstance().addContact(contactModelBuilder.getContactModel());
            stringBuilder.append("\nEND--Contact");
            this.contactModelBuilder = new ContactModel.ContactModelBuilder();
            clearFlags();

            return stringBuilder.toString();
        }

        if(nodeName.equals(Group.getTagName())){
            GroupCollection.getInstance().addGroup(groupBuilder.getGroup());
            stringBuilder.append("\nEND--Group");
            this.groupBuilder = new Group.GroupBuilder();
            clearFlags();;

            return stringBuilder.toString();
        }

        // If we are here we expect empty string
        return stringBuilder.toString();
    }

    private  void clearFlags(){
        this.isGroupTag =false;
        this.isContactTag= false;
        this.areAttrsCollected = false;
    }

    // To potential refactor change parser to String Value
    private String contactAddFragment(XmlPullParser parser,
                                      String nodeName,
                                      ContactModel.ContactModelBuilder contactBuilder){
        StringBuilder stringBuilder = new StringBuilder();

        switch (nodeName){

            case "name": contactBuilder.setName(parser.getText());
                stringBuilder.append("\n"+nodeName).append(": ").append(parser.getText());
                break;

            case "surname": contactBuilder.setSurname(parser.getText());
                stringBuilder.append("\n"+nodeName).append(": ").append(parser.getText());
                break;

            case "webPage": contactBuilder.setWebPage(parser.getText()); // case ?
                stringBuilder.append("\n"+nodeName).append(": ").append(parser.getText());
                break;

            case "address": contactBuilder.setAddress(parser.getText());
                stringBuilder.append("\n"+nodeName).append(": ").append(parser.getText());
                break;

            default:
                break;
        }
        return stringBuilder.toString();
    }

    private String groupAddFragment(XmlPullParser parser,
                                    String nodeName,
                                    Group.GroupBuilder groupBuilder){

        StringBuilder stringBuilder = new StringBuilder();

        switch (nodeName){
            case "name": groupBuilder.setName(parser.getText());
                stringBuilder.append("\n"+nodeName).append(": ").append(parser.getText());
                break;

            case "contactID": groupBuilder.addContactToGroup(Integer.parseInt(parser.getText()));
                stringBuilder.append("\n"+nodeName).append(": ").append(parser.getText());
                break;

            default:
                break;
        }

        return stringBuilder.toString();
    }

    private String getAttributesContact(XmlPullParser parser,
                                        ContactModel.ContactModelBuilder contactModelBuilder){

        StringBuilder stringBuilder = new StringBuilder();
        String name = parser.getName();

        stringBuilder.append("START--Group");

        if(name !=null){
            int size = parser.getAttributeCount();
            for(int i =0;i<size;i++){
                String attrName = parser.getAttributeName(i);

                if(attrName.equals("sex")){
                    String attrValue = parser.getAttributeValue(i);
                    contactModelBuilder.setSex(attrValue);
                    stringBuilder.append("\nContact Attribute: sex | Value: ").append(attrValue);
                }
                else if(attrName.equals("id")){
                    int attrValue = Integer.parseInt(parser.getAttributeValue(i));
                    contactModelBuilder.setId(attrValue);
                    stringBuilder.append("\nContact Attribute: id | Value: ").append(attrValue);
                }
            }
        }
        return stringBuilder.toString();
    }

    public String getReaderResult(){
        return this.readerResult;
    }
    public void Read(Context context, InputStream input) throws ExecutionException, InterruptedException {
            ReadAsync reader = new ReadAsync(input,context);
            reader.execute(input).get();
    }

    class ReadAsync extends AsyncTask<InputStream,Void,String> {
        InputStream input;
        ProgressDialog dialog;

        public ReadAsync(InputStream input, Context context) {
            this.input = input;
            this.dialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            dialog.dismiss();
            readerResult = result;
            Log.wtf("\nAsync Read Result: ",result);
        }

        @Override
        protected String doInBackground(InputStream... inputStreams) {
            InputStream input = inputStreams[0];
            String resultStr ="";

            resultStr = _read(input);

            return resultStr;
        }
    }
}
