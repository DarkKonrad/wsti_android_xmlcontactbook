package com.example.xmladdresbook.forms;

import android.graphics.Color;
import android.text.Layout;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Space;

import com.example.xmladdresbook.Collections.GroupCollection;
import com.example.xmladdresbook.model.ContactModel;
import com.example.xmladdresbook.model.Group;

import java.util.ArrayList;

public class BookEntryRow {

    LinearLayout layout;

    Row name, surname, address, phoneNumber, webPage, email,sex, group;
    Space bottomSpace;
    ContactModel model;

    public BookEntryRow(LinearLayout parent, ContactModel model){
        this.model = model;
        prepareContents(parent);
    }

    public BookEntryRow(LinearLayout parent){
        prepareBlank(parent);
        model = null;
    }

    private void prepareBlank(LinearLayout parent){
        layout = new LinearLayout(parent.getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ));

        parent.addView(layout);
        layout.setVisibility(View.VISIBLE);

        name = new Row(layout,"Name: ","",false);
        surname = new Row(layout,"Surname: ","",false);
        address = new Row(layout,"Address: ", "",false);
        phoneNumber = new Row(layout,"Phone Number: ","",false);
        webPage = new Row(layout,"Web Page: ","",false);
        email = new Row(layout,"E-mail: ", "",false);
        sex = new Row(layout,"Sex: ","",false);
        group = new Row(layout,"Group: ","",false);

        bottomSpace = new Space(layout.getContext());
        bottomSpace.setBackgroundColor(Color.GRAY);
        bottomSpace.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                30));

        layout.addView(bottomSpace);

    }


    private void prepareContents(LinearLayout parent){
        layout = new LinearLayout(parent.getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ));

        parent.addView(layout);
        layout.setVisibility(View.VISIBLE);

        name = new Row(layout,"Name: ",model.getName());
        surname = new Row(layout,"Surname: ",model.getSurname());
        address = new Row(layout,"Address: ", model.getAddress());
        phoneNumber = new Row(layout,"Phone Number: ",model.getPhoneNumber());
        webPage = new Row(layout,"Web Page: ",model.getWebPage());
        email = new Row(layout,"E-mail: ", model.getEmail());
        sex = new Row(layout,"Sex: ",model.getSex());

        StringBuilder stringBuilder = new StringBuilder();
        ArrayList<Group> listOfGroups= GroupCollection.getInstance().findGroupsByContact(model);

        for(Group element : listOfGroups){
            stringBuilder.append(element.getName()).append(" ");
        }

        group = new Row(layout,"Group: ",stringBuilder.toString());

        bottomSpace = new Space(layout.getContext());
        bottomSpace.setBackgroundColor(Color.GRAY);
        bottomSpace.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                30));

        layout.addView(bottomSpace);

    }

    public String getName() {
        return name.getContents();
    }

    public String getSurname() {
        return surname.getContents();
    }

    public String getAddress() {
        return address.getContents();
    }

    public String getPhoneNumber() {
        return phoneNumber.getContents();
    }

    public String getWebPage() {
        return webPage.getContents();
    }

    public String getEmail() {
        return email.getContents();
    }

    public String getSex() {
        return sex.getContents();
    }

    public String[] getGroup() {
        return group.getContents().split(" ");
    }


    public Row getNameRow() {
        return name;
    }

    public Row getSurnameRow() {
        return surname;
    }

    public Row getAddressRow() {
        return address;
    }

    public Row getPhoneNumberRow() {
        return phoneNumber;
    }

    public Row getWebPageRow() {
        return webPage;
    }

    public Row getEmailRow() {
        return email;
    }

    public Row getSexRow() {
        return sex;
    }

    public Row getGroupRow() {
        return group;
    }
}
