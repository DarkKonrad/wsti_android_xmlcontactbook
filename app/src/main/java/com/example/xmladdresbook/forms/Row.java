package com.example.xmladdresbook.forms;

import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Space;

public class Row {
    LinearLayout row;
    EditText title, contents;
    Space space;
    String strTitle, strContents;

    /// Contents are locked by default
    public Row (LinearLayout parent, String Title, String Contents){
        this.strContents = Contents;
        this.strTitle = Title;
        prepareContents(parent);
        lockContents();
    }

    public Row (LinearLayout parent, String Title, String Contents, boolean lock){
        this.strContents = Contents;
        this.strTitle = Title;
        prepareContents(parent);
        if(lock == true)
            lockContents();
        else
            unlockContents();
    }
    public String getTitle(){
        return strTitle;
    }

    public String getContents(){
        return contents.getText().toString();
    }

    public void lockContents(){
        contents.setClickable(false);
        contents.setFocusable(false);
    }

    public void unlockContents(){
        contents.setClickable(true);
        contents.setFocusable(true);
    }


    private void prepareContents(LinearLayout parent){
        // Whole 'row' aka 'Widget'
        row = new LinearLayout(parent.getContext());
        row.setOrientation(LinearLayout.HORIZONTAL);
        row.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        ));

        parent.addView(row);
        row.setVisibility(View.VISIBLE);

        // Title EditText
        title = new EditText((row.getContext()));
        title.setMaxLines(1);
        title.setClickable(false);
        title.setFocusable(false);
        title.setText(this.strTitle);

        row.addView(title);

        // Space between Title and Contents
        space = new Space(row.getContext());
        space.setLayoutParams(new LinearLayout.LayoutParams(
                200,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ));

        row.addView(space);

        //Contents EditText
        contents = new EditText(row.getContext());

        contents.setText(this.strContents);
        row.addView(contents);

    }

}
