package com.example.xmladdresbook.model;

import com.example.xmladdresbook.XMLHandling.XmlNode;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Group implements Serializable, XmlNode {

  private static int _id = 0;
  private int id;
  private String name;
  private ArrayList<Integer> contactsID;

  private static String tagName = "Group";
  public Group(String name){
        this.id = ++_id;
        this.name = name;
        contactsID = new ArrayList<>();
    }

    private Group(int id,String name, ArrayList<Integer> contactsID){
      this.id =id;
      this.name = name;
      this.contactsID = contactsID;
    }
    public void addContactToGroup(int ContactID){
      contactsID.add(ContactID);
    }

    public boolean isContactInGroup(int ContactID){
      for(Integer i : contactsID){
          if (ContactID == i){
              return true;
          }
      }
      return false;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static String getTagName() {return tagName;}

    @Override
    public Node getXmlNode(Document doc) {
        Element group = doc.createElement(tagName);
        group.setAttribute("id",Integer.toString(id));

        Element groupName = doc.createElement("name");
        groupName.appendChild(doc.createTextNode(name));
        group.appendChild(groupName);

        for (Integer i : contactsID){

            Element contactID = doc.createElement("contactID");
            contactID.appendChild(doc.createTextNode(i.toString()));
            group.appendChild(contactID);
        }

        return group;
    }

    public static class GroupBuilder{
        private int id;
        private String name;
        private ArrayList<Integer> contactsID;

        public GroupBuilder(){
            contactsID = new ArrayList<>();
            name ="";
            id = -1;
        }

        public Group getGroup(){
            return new Group(id,name,contactsID);
        }

        public void addContactToGroup(int ContactID){
            contactsID.add(ContactID);
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
//    private Node getXmlElement(Document doc){
//      Element node = doc.createElement("name");
//      node.appendChild(doc.createTextNode(name));
//      return node;
//    }

}
