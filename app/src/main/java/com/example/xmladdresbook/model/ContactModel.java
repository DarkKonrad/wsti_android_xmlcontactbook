package com.example.xmladdresbook.model;

import com.example.xmladdresbook.XMLHandling.XmlNode;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ContactModel implements Serializable, XmlNode {

    private static int _id;
    private String name, surname, webPage,phoneNumber,address, email, sex;
    private  int id;
    private static String tagName = "Contact";

//    public ContactModel(String name, String surname, String webPage, String phoneNumber, String address, String email, String sex,  Group group) {
//        this.name = name;
//        this.surname = surname;
//        this.webPage = webPage;
//        this.phoneNumber = phoneNumber;
//        this.address = address;
//        this.email = email;
//        this.sex = sex;
//        this.id = ++_id;
//    }

    public ContactModel(String name, String surname, String webPage, String phoneNumber, String address, String email, String sex) {
        this.name = name;
        this.surname = surname;
        this.webPage = webPage;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.email = email;
        this.sex = sex;
        this.id = ++_id;

    }
    private ContactModel(int id,String name, String surname, String webPage, String phoneNumber, String address, String email, String sex) {
        this.name = name;
        this.surname = surname;
        this.webPage = webPage;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.email = email;
        this.sex = sex;
        this.id = id;

    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getId() {
        return id;
    }

    public String getWebPage() {
        return webPage;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getSex() {
        return sex;
    }

    public static String getTagName() {return tagName;}

    public void addToGroup(Group g){
        g.addContactToGroup(id);
}

    @Override
    public Node getXmlNode(Document doc) {
        final Element contact =  doc.createElement(tagName);

        contact.setAttribute("id",Integer.toString(id));
        contact.setAttribute("sex",sex);
        contact.appendChild(getXmlElement(doc,"name",name));
        contact.appendChild(getXmlElement(doc,"surname", surname));
        contact.appendChild(getXmlElement(doc,"webPage", webPage));
        contact.appendChild(getXmlElement(doc,"address", address));

        return  contact;
    }

    private Node getXmlElement(Document doc,String name, String value){
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));

        return node;
    }

  public static class ContactModelBuilder{
        private String name , surname, webPage,phoneNumber,address, email, sex;
        private  int id;

        public ContactModelBuilder() {
            name = "";
            surname = "";
            webPage = "";
            phoneNumber = "";
            address = "";
            email = "";
            sex = "";
            id = 0;
        }

        public ContactModel getContactModel(){
            if(this.id <= 0 )
                return  new ContactModel(name,surname,webPage,phoneNumber,address,email,sex);

            return new ContactModel(id,name,surname,webPage,phoneNumber,address,email,sex);
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public void setWebPage(String webPage) {
            this.webPage = webPage;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
