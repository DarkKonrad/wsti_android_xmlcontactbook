package com.example.xmladdresbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.xmladdresbook.Collections.ContactCollection;
import com.example.xmladdresbook.XMLHandling.XMLReader;
import com.example.xmladdresbook.filemanager.FileManager;
import com.example.xmladdresbook.forms.BookEntryRow;
import com.example.xmladdresbook.model.ContactModel;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    Button addButton,editButton,deleteButton;
    LinearLayout scrollViewLayout;
    ArrayList<BookEntryRow> rowsCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prepareUIHandlers();

        rowsCollection = new ArrayList<>();
        ContactCollection contactCollection = ContactCollection.getInstance();

        FileManager manager = FileManager.getInstance();
        manager.setContext(this);
        FileOutputStream contactXml = manager.getFileOutputStream();


        try {
            XMLReader.getInstance().Read(this,
                    manager.getFileInputStream());
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }

        catch (InterruptedException e) {
            e.printStackTrace();
        }

        //   manager._DebugWriteFileContents();
        Log.e("---Result String---", XMLReader.getInstance().getReaderResult());
        for(ContactModel contact : contactCollection.getContactsContainer()){
            rowsCollection.add(
                    new BookEntryRow(scrollViewLayout,contact)
            );

        }



    }

    void prepareUIHandlers(){
        scrollViewLayout = (LinearLayout)findViewById(R.id.scrollViewLayout);
        addButton = (Button) findViewById(R.id.btnAdd);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,ContactAvtivity.class);
                startActivity(i);
            }
        }); // end addButton


    }
}
