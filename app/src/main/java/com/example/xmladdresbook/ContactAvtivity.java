package com.example.xmladdresbook;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.TypedArrayUtils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.xmladdresbook.Collections.ContactCollection;
import com.example.xmladdresbook.Collections.GroupCollection;
import com.example.xmladdresbook.XMLHandling.XMLWriter;
import com.example.xmladdresbook.XMLHandling.XmlNode;
import com.example.xmladdresbook.filemanager.FileManager;
import com.example.xmladdresbook.forms.BookEntryRow;
import com.example.xmladdresbook.model.ContactModel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ContactAvtivity extends AppCompatActivity {
BookEntryRow row;
LinearLayout layout;
Button saveButton,backButton;
ContactModel.ContactModelBuilder contactModelBuilder;
ContactCollection contactCollectionInst;
GroupCollection groupCollectionInst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_avtivity);

        contactCollectionInst = ContactCollection.getInstance();
        groupCollectionInst = GroupCollection.getInstance();


        layout = (LinearLayout) findViewById(R.id.mainLayout);
        saveButton = (Button) findViewById(R.id.btnSave);
        backButton = (Button) findViewById(R.id.btnBack);

        row = new BookEntryRow(layout);

        saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                contactModelBuilder = new ContactModel.ContactModelBuilder();

                contactModelBuilder.setName(row.getName());
                contactModelBuilder.setAddress(row.getAddress());
                contactModelBuilder.setWebPage(row.getWebPage());
                contactModelBuilder.setSurname(row.getSurname());
                contactModelBuilder.setEmail(row.getEmail());
                contactModelBuilder.setSex(row.getSex());

                ContactModel model = contactModelBuilder.getContactModel();
                contactCollectionInst.addContact(model);

                String[] groups = row.getGroup();
                for(String element : groups){
                    groupCollectionInst.addContactToGroup(element,model);
                }

                XMLWriter.getInstance().addXmlNodesToWrite(contactCollectionInst.getXmlNodeArray());
                XMLWriter.getInstance().addXmlNodesToWrite(groupCollectionInst.getXmlNodeArray());

                XMLWriter.getInstance().write(ContactAvtivity.this,
                        FileManager.getInstance().getFileOutputStream(true));

                Intent i = new Intent(ContactAvtivity.this,MainActivity.class);
                startActivity(i);
            }
        }); // end saveButton

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ContactAvtivity.this,MainActivity.class);
                startActivity(i);
            }
        }); //end backButton;


    }
}
