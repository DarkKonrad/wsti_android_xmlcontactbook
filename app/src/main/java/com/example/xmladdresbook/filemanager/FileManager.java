package com.example.xmladdresbook.filemanager;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class FileManager {
    static final String fileName ="Contacts.xml";
    String path,directory;
    Context context;

    private static FileManager fileManager = new FileManager();

    private FileManager( ) {context= null;}

    public void setContext(Context context){
        this.context = context;
        path = context.getFilesDir().getPath();
        directory = context.getFilesDir().getPath();
    }

    public static FileManager getInstance(){
        return fileManager;
    }
    public String getFileName(){
        return fileName;
    }
    public boolean isContextNotNull(){
        return context != null;
    }

    private void _isContextNotNull() throws Exception{
        if(context == null)
            throw new Exception("FileManager Context is null");
    }

    public FileOutputStream getFileOutputStream() {
        try{
            _isContextNotNull();
            return context.openFileOutput(fileName,Context.MODE_APPEND);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null; // just to make compiler satisified
    }

    public FileOutputStream getFileOutputStream(boolean erase) {
        try{
            _isContextNotNull();
            if(erase){
                context.openFileOutput(fileName,Context.MODE_PRIVATE).close();
                context.deleteFile(fileName);
            }

            return context.openFileOutput(fileName,Context.MODE_APPEND);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null; // just to make compiler satisified
    }



    public FileInputStream getFileInputStream() {
        try{
            _isContextNotNull();
            return context.openFileInput(fileName);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
