package com.example.xmladdresbook.Collections;

import com.example.xmladdresbook.XMLHandling.XmlNode;
import com.example.xmladdresbook.model.ContactModel;
import com.example.xmladdresbook.model.Group;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class GroupCollection implements XmlNode {
    // At second thought, is may be overkill
    Map<String, Group> groupCollectionContainer,backup;

    private static GroupCollection groupCollection = new GroupCollection();

    private GroupCollection() {
        groupCollectionContainer = new HashMap<>();
        backup = null;
    }

    public void addContactToGroup(String groupName, ContactModel contact){
        // find group
        Group group = findGroupByName(groupName);
        // If there is no group, create a new one
        if(group == null){
            group = addGroup(groupName);
        }
        group.addContactToGroup(contact.getId());
    }

public Group[] getGroupsArray(){
   Group [] arr = new Group[groupCollectionContainer.size()];
   int i =0;

    for (Map.Entry<String, Group> entry : groupCollectionContainer.entrySet()) {
        Group val = entry.getValue();
        arr[i] = val;
        i++;
    }

    return arr;
}

public  XmlNode[] getXmlNodeArray(){
    XmlNode [] arr = new Group[groupCollectionContainer.size()];
    int i =0;

    for (Map.Entry<String, Group> entry : groupCollectionContainer.entrySet()) {
        Group val = entry.getValue();
        arr[i] = (XmlNode)val;
        i++;
    }

    return arr;
}
    public static GroupCollection getInstance() {
        return groupCollection;
    }

    public void addGroup(Group group) {
        groupCollectionContainer.put(group.getName(), group);
    }

    public Group addGroup(String name) {
        Group group = new Group(name);
        groupCollectionContainer.put(name, group);
        return group;
    }

    public Group findGroupByName(String name) {
        return groupCollectionContainer.get(name);
    }

    public Group findGroupByID(int id) { // There is no lambda in API v 23 :/
        for (Map.Entry<String, Group> entry : groupCollectionContainer.entrySet()) {
            Group val = entry.getValue();
            if (val.getId() == id)
                return val;
        }
        return null;
    }

    public void removeGroup(String name) {
        groupCollectionContainer.remove(name);
    }
    public void removeGroup(Group group){
        removeGroup(group.getName());
    }
    public void removeAll(){
        groupCollectionContainer.clear();
    }

    public ArrayList<Group> findGroupsByContact(ContactModel model){
        int id = model.getId();
        ArrayList<Group> listOfGroups = new ArrayList<>();
        for (Map.Entry<String, Group> entry : groupCollectionContainer.entrySet()) {
            Group val = entry.getValue();
            if (val.isContactInGroup(id)){
                listOfGroups.add(val);
            }

        }
        return listOfGroups;
    }

    public Map<String, Group> getGroupCollectionContainer() {return groupCollectionContainer;}

    public boolean isGroupExisits(String name) {
        return groupCollectionContainer.containsKey(name);
    }

    public void makeBackup(){
        backup = new HashMap<>(groupCollectionContainer);
    }
    public void restoreBackup(){
        if(backup!=null)
            groupCollectionContainer = new HashMap<>(backup);
        backup = null;
    }

    @Override
    public Node getXmlNode(Document doc) {
        Element groupsRoot = doc.createElement("Groups");
        for (Map.Entry<String, Group> entry : groupCollectionContainer.entrySet()) {
            Group val = entry.getValue();
            groupsRoot.appendChild(val.getXmlNode(doc));
        }
        return groupsRoot;
    }
}