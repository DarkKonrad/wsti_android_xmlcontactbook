package com.example.xmladdresbook.Collections;

import com.example.xmladdresbook.XMLHandling.XmlNode;
import com.example.xmladdresbook.model.ContactModel;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

public final class ContactCollection implements XmlNode {
ArrayList<ContactModel> contactCollectionContainter,backup;

    private static ContactCollection contactCollection = new ContactCollection();
    private ContactCollection(){
        contactCollectionContainter = new ArrayList<>();
    }

    public static ContactCollection getInstance(){
        return  contactCollection;
    }

    public void addContact(ContactModel model){
        contactCollectionContainter.add(model);
    }
    public void removeContact(ContactModel model){
        contactCollectionContainter.remove(model);
    }
    public void removeContact(int id){
        contactCollectionContainter.remove(findContactById(id));
    }
    public void removeAll(){
        contactCollectionContainter.clear();
    }
    public XmlNode[] getXmlNodeArray(){
        List list = contactCollectionContainter;
        XmlNode[] arr = new ContactModel[contactCollectionContainter.size()];
        arr = (XmlNode[]) list.toArray(arr);

        return arr;
    }

    // To potential refactor return some manner of iterator
    public ArrayList<ContactModel> getContactsContainer() {return  contactCollectionContainter;}

    public ContactModel findContactById(int id){
        for(ContactModel model : contactCollectionContainter){
            if(model.getId()==id){
                return model;
            }
        }
        return  null;
    }

    public void makeBackup(){
            backup = (ArrayList<ContactModel>) contactCollectionContainter.clone();
    }

    public void restoreBackup(){
        if(backup != null)
            contactCollectionContainter = (ArrayList<ContactModel>) backup.clone();
        backup = null;
    }

    @Override
    public Node getXmlNode(Document doc) {
        Element contactRoot = doc.createElement("Contacts");
        for(ContactModel model : contactCollectionContainter){
            contactRoot.appendChild(model.getXmlNode(doc));
        }
        return contactRoot;
    }
}
